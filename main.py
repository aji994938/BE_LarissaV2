from flask import Flask, render_template, Response, request, abort, jsonify, flash, redirect, url_for
from flask_cors import CORS
from flask_httpauth import HTTPTokenAuth
from werkzeug.exceptions import HTTPException
from sqlalchemy.exc import SQLAlchemyError
from flask_bcrypt import Bcrypt, check_password_hash
from werkzeug.utils import secure_filename
from pytz import timezone
from sqlalchemy import text, bindparam
from google.cloud import storage
from datetime import datetime as dt
from datetime import date, timedelta
# from flask_jwt_extended import JWTManager, create_access_token, jwt_required, get_jwt_identity
from flask_jwt_extended.exceptions import NoAuthorizationError
from psycopg2.errors import UniqueViolation
from sqlalchemy.exc import SQLAlchemyError, IntegrityError
import random
import string
import sqlalchemy
import os
import json
import datetime
import hashlib
import collections
import pytz
import re
import sys


UPLOAD_FOLDER = 'uploads'
ALLOWED_EXTENSIONS = {'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'}

# FLASK INTIALIZATION
app = Flask(__name__)
bcrypt = Bcrypt(app)
CORS(app, resources={r'/*': {'origins': '*'}})
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

# # Configure JWT
# app.config['JWT_SECRET_KEY'] = 'H0rus_H4rus_H3roes' 
# app.config['JWT_ACCESS_TOKEN_EXPIRES'] = timedelta(days=1) 
# jwt = JWTManager(app)

# ---------------------------------------------------------------------------------
# DATABASE INTIALIZATION
# ---------------------------------------------------------------------------------

def init_connection_engine():
    db_config = {
        # [START cloud_sql_mysql_sqlalchemy_limit]
        # Pool size is the maximum number of permanent connections to keep.
        "pool_size": 25,
        # Temporarily exceeds the set pool_size if no connections are available.
        "max_overflow": 5,
        # The total number of concurrent connections for your application will be
        # a total of pool_size and max_overflow.
        # [END cloud_sql_mysql_sqlalchemy_limit]

        # [START cloud_sql_mysql_sqlalchemy_backoff]
        # SQLAlchemy automatically uses delays between failed connection attempts,
        # but provides no arguments for configuration.
        # [END cloud_sql_mysql_sqlalchemy_backoff]

        # [START cloud_sql_mysql_sqlalchemy_timeout]
        # 'pool_timeout' is the maximum number of seconds to wait when retrieving a
        # new connection from the pool. After the specified amount of time, an
        # exception will be thrown.
        "pool_timeout": 30,  # 30 seconds
        # [END cloud_sql_mysql_sqlalchemy_timeout]

        # [START cloud_sql_mysql_sqlalchemy_lifetime]
        # 'pool_recycle' is the maximum number of seconds a connection can persist.
        # Connections that live longer than the specified amount of time will be
        # reestablished
        "pool_recycle": 1800,  # 30 minutes
        # [END cloud_sql_mysql_sqlalchemy_lifetime]

    }
    if os.environ.get("DB_HOST"):
        if os.environ.get("DB_ROOT_CERT"):
            return init_tcp_sslcerts_connection_engine(db_config)
        return init_tcp_connection_engine(db_config)

    return init_unix_connection_engine(db_config)
    # return init_tcp_connection_engine(db_config)

def init_tcp_sslcerts_connection_engine(db_config):
    # [START cloud_sql_mysql_sqlalchemy_create_tcp_sslcerts]
    # Remember - storing secrets in plaintext is potentially unsafe. Consider using
    # something like https://cloud.google.com/secret-manager/docs/overview to help keep
    # secrets secret.
    db_user = os.environ["DB_USER"]
    db_pass = os.environ["DB_PASS"]
    db_name = os.environ["DB_NAME"]
    db_host = os.environ["DB_HOST"]
    db_root_cert = os.environ["DB_ROOT_CERT"]
    db_cert = os.environ["DB_CERT"]
    db_key = os.environ["DB_KEY"]

    # Extract port from db_host if present,
    # otherwise use DB_PORT environment variable.
    host_args = db_host.split(":")
    if len(host_args) == 1:
        db_hostname = host_args[0]
        db_port = int(os.environ["DB_PORT"])
    elif len(host_args) == 2:
        db_hostname, db_port = host_args[0], int(host_args[1])

    ssl_args = {
        "ssl_ca": db_root_cert,
        "ssl_cert": db_cert,
        "ssl_key": db_key
    }

    pool = sqlalchemy.create_engine(
        # Equivalent URL:
        # mysql+pymysql://<db_user>:<db_pass>@<db_host>:<db_port>/<db_name>
        sqlalchemy.engine.url.URL.create(
            drivername="postgresql+pg8000",
            username=db_user,  # e.g. "my-database-user"
            password=db_pass,  # e.g. "my-database-password"
            host=db_hostname,  # e.g. "127.0.0.1"
            port=db_port,  # e.g. 3306
            database=db_name  # e.g. "my-database-name"
        ),
        connect_args=ssl_args,
        **db_config
    )
    # [END cloud_sql_mysql_sqlalchemy_create_tcp_sslcerts]

    return pool

def init_tcp_connection_engine(db_config):
    # [START cloud_sql_mysql_sqlalchemy_create_tcp]
    # Remember - storing secrets in plaintext is potentially unsafe. Consider using
    # something like https://cloud.google.com/secret-manager/docs/overview to help keep
    # secrets secret.
    # db_user = os.environ["DB_USER"]
    # db_pass = os.environ["DB_PASS"]
    # db_name = os.environ["DB_NAME"]
    # db_host = os.environ["DB_HOST"]
    # db_user = 'postgres'
    # db_pass = 'horus'
    # db_name = 'larissav2'
    # db_host = 'localhost'

    db_user = 'postgres'
    db_pass = '123'
    db_name = 'larissav2_customermember_db'
    db_host = 'localhost'

    # Extract port from db_host if present,
    # otherwise use DB_PORT environment variable.
    host_args = db_host.split(":")
    if len(host_args) == 1:
        db_hostname = db_host
        # db_port = os.environ["DB_PORT"]
        db_port = '5432'
    elif len(host_args) == 2:
        db_hostname, db_port = host_args[0], int(host_args[1])

    pool = sqlalchemy.create_engine(
        # Equivalent URL:
        # mysql+pymysql://<db_user>:<db_pass>@<db_host>:<db_port>/<db_name>
        sqlalchemy.engine.url.URL.create(
            drivername="postgresql+pg8000",
            username=db_user,  # e.g. "my-database-user"
            password=db_pass,  # e.g. "my-database-password"
            host=db_hostname,  # e.g. "127.0.0.1"
            port=db_port,  # e.g. 3306
            database=db_name,  # e.g. "my-database-name"
        ),
        **db_config
    )
    # [END cloud_sql_mysql_sqlalchemy_create_tcp]

    return pool

def init_unix_connection_engine(db_config):
    # [START cloud_sql_mysql_sqlalchemy_create_socket]
    # Remember - storing secrets in plaintext is potentially unsafe. Consider using
    # something like https://cloud.google.com/secret-manager/docs/overview to help keep
    # secrets secret.
    db_user = os.environ["DB_USER"]
    db_pass = os.environ["DB_PASS"]
    db_name = os.environ["DB_NAME"]
    #db_socket_dir = os.environ.get("DB_SOCKET_DIR", "/cloudsql") 
    #instance_connection_name = os.environ["INSTANCE_CONNECTION_NAME"]
    unix_socket_path = os.environ["INSTANCE_UNIX_SOCKET"]  # e.g. '/cloudsql/project:region:instance'

    pool = sqlalchemy.create_engine(
        # Equivalent URL:
        # mysql+pymysql://<db_user>:<db_pass>@/<db_name>?unix_socket=<socket_path>/<cloud_sql_instance_name>
        sqlalchemy.engine.url.URL.create(
            drivername="postgresql+pg8000",
            username=db_user,  # e.g. "my-database-user"
            password=db_pass,  # e.g. "my-database-password"
            database=db_name,  # e.g. "my-database-name"
            # query={
            #     "unix_socket": "{}/{}".format(
            #         db_socket_dir,  # e.g. "/cloudsql"
            #         instance_connection_name)  # i.e "<PROJECT-NAME>:<INSTANCE-REGION>:<INSTANCE-NAME>"
            # }
            query={"unix_sock": "{}/.s.PGSQL.5432".format(unix_socket_path)},
        ),
        **db_config
    )
    # [END cloud_sql_mysql_sqlalchemy_create_socket]

    return pool

# c = init_tcp_connection_engine()
db  = init_connection_engine()
token_auth = HTTPTokenAuth()

# ---------------------------------------------------------------------------------
# FUNCTIONS
# ---------------------------------------------------------------------------------

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.errorhandler(HTTPException)
def handle_exception(e):
    '''
        Formatting HTTP Exception error into a dict.
        @require `HTTPException` from werkzeug.exceptions.
        @param `e` an [object] of HTTPExeception Genereted when Error Raised.
        @returns `data` a [dict] of formated error message.
    '''
    response = e.get_response()
    response.data = json.dumps({
        "result" : [{"message" : e.name + " : " + e.description}],
        "status" : e.code,
    })
    response.content_type = "application/json"

    return response


@token_auth.verify_token
def verify_token(token):
    '''
        Performing HTTP Authentication before Taking Request.
        @require `HTTPTokenAuth` from flask_httpauth.
        @param `token` a [string] of Token Used to Validate.
        @returns `token`| `HTTPException 403`.
    '''
    with db.connect() as conn:
        query  = text("SELECT token FROM karyawan WHERE token=:s_token")
        # where  = (token)
        query  = query.bindparams(s_token=token)
        result = conn.execute(query).fetchone()
        rtoken  = result[0] if result else None

    return rtoken if token else abort(403)

# @app.errorhandler(NoAuthorizationError)
# def handle_no_auth_error(error):
#     return set({"message" : "Token tidak ada atau tidak valid"}), 401

# @jwt.expired_token_loader
# def handle_token_expired(jwt_header, jwt_payload):
#     return set({"message" : "Token telah kedaluwarsa"}), 401

def set(result):
    '''
        Setting up database fetched result into formated dict.
        @param `result` an [object] of Fetched Result or a [dict] of result message.
        @returns `data` a [dict] of formated result which ready to send as a response call back. 
    '''
    data = []

    if (isinstance(result, dict) == True):
        data.append(result)

    elif len(list(result)) > 0 :
        for i in result:
            data.append(i._asdict())

    data = {
        "result" : data,
        "status" : 200
    }

    return data

def setLogin(result):
    '''
        Setting up database fetched result into formated dict.
        @param `result` an [object] of Fetched Result or a [dict] of result message.
        @returns `data` a [dict] of formated result which ready to send as a response call back. 
    '''
    data = []

    if (isinstance(result, dict) == True):
        data.append(result)

    elif len(list(result)) > 0 :
        for i in result:
            data.append(i._asdict())

    return data

def handle_error(e):
    if isinstance(e, IntegrityError):
        if 'duplicate key value violates unique constraint' in str(e.orig):
            if 'Key' in str(e.orig):
                field_name = str(e.orig).split('Key (')[1].split(')=')[0].capitalize()
                return set({"message" : f"{field_name} sudah terdaftar."}), 409

        # General IntegrityError handling
        return set({"message" : "Terjadi pelanggaran integritas data. Periksa kembali input Anda."}), 400

    # Handle any other types of exceptions
    return set({"message" : "Terjadi kesalahan yang tidak terduga."}), 500

def null_date(value):
    '''
        Formatting `Null | None` Date Result. (Only Date)
        @param `value` a [string] of Date Want to be Formatted.
        @result '0001-01-01' | None | [string] Date.
    '''
    # val_str = value.decode('utf-8')
    if value == '0000-00-00' or value == '0000-01-01' or value == '' :
        return '0001-01-01'
    elif value is None :
        return None
    else :
        return datetime.datetime.strftime(value, '%Y-%m-%d')


def null_datetime(value):
    '''
        Formatting `Null | None` Date Time Result.
        @param `value` a [string] of Date Time Want to be Formatted.
        @result '0001-01-01' | None | [string] Date Time.   
    '''
    # val_str = value.decode('utf-8')  
    if value == '0000-00-00 00:00:00' or value == '' :
        return '0001-01-01 00:00:00'
    elif value is None :
        return None
    else :
        return datetime.datetime.strftime(value, '%Y-%m-%d %H:%M:%S')


def insert_nulldatetime(value):
    '''
        Function insert `Null | None` Date Time untuk Date yang Ada Time.
    '''
    if value is None or value == "" :
        return None


def insert_nulldatetime_required(value):
    '''
        Insert untuk Date Time yang Wajib Diisi | require.
        @return '0001-01-01 00:00:00'
    '''
    if value is None or value == "" :
        return '0001-01-01 00:00:00'


def timeZone():
    '''
        Format Timezone untuk Asia/Jakarta  
    '''
    fmt       = "%Y-%m-%d %X"
    nowutc    = datetime.datetime.now(timezone('UTC'))
    date_now  = nowutc.astimezone(timezone('Asia/Jakarta'))

    return str(date_now.strftime(fmt))


def timeZoneDate():
    '''
        Format Timezone untuk Asia/Jakarta Khusus Tanggal Saja.
    '''
    fmt       = "%Y-%m-%d"
    nowutc    = datetime.datetime.now(timezone('UTC'))
    date_now  = nowutc.astimezone(timezone('Asia/Jakarta'))

    return str(date_now.strftime(fmt))


def timeZoneTime():
    '''
        Format Timezone untuk Asia/Jakarta Khusus Waktu Saja.
    '''
    fmt       = "%X"
    nowutc    = datetime.datetime.now(timezone('UTC'))
    date_now  = nowutc.astimezone(timezone('Asia/Jakarta'))

    return str(date_now.strftime(fmt))


def randomword(length):
    '''
        Function to Create Random String.
        @param `length` an [int] of sting length.
        @return `string`
    '''
    letters = string.ascii_lowercase + string.ascii_uppercase + string.digits

    return ''.join(random.choice(letters) for i in range(length))


def changetypedate(datee) :

    year, month, day = map(int, datee.split('-'))
    date_obj = date(year, month, day)
    return date_obj


# ---------------------------------------------------------------------------------
# BASE API
# ---------------------------------------------------------------------------------

@app.route('/api/base/<tablename>', methods=['GET', 'POST', 'PUT', 'DELETE'])
# # @token_auth.login_required
def base(tablename):
    '''
        Base API for CRUD Only from One Table.
        @use `pg8000` Python Database Engine for Postgree.
        @method `Query Builders` Where, WhereOr, OrderBy, Limit, Returning.
        @method `GET` `Fetching` Requested Data from Database.
        @method `POST` `Inserting` Data to Database.
        @method `PUT` `Update` Requested Data to Database.
        @method `DELETE` `Delete` Requested Data to Database.
        @required `token_auth` Triggering Token Verification.
    '''

    with db.connect() as conn:

    # Additional Clause Builder (From Request Arguments)
    # --------------------------------------------------
        query     = ""
        where     = request.args.get('where')
        whereOr   = request.args.get('whereOr')
        orderBy   = request.args.get('orderBy')
        limit     = request.args.get('limit')
        returning = request.args.get('returning')

        if  where : # Format `Where` Clause
            where = json.loads(where)
            for key, value in where.items() :
                query += key + value
                if key != list(where)[-1] :
                    query += " AND "

        if  whereOr : # Format `Where Or` Clause
            whereOr = json.loads(whereOr)
            if query != "" :
                query += " OR "
            for key, value in whereOr.items() :
                query += key + value
                if key != list(whereOr)[-1] :
                    query += " OR "

        if  where or whereOr : # Add `Where | Where Or` Clause
            query = " WHERE " + query

        if  orderBy : # Add `Order By` Clause
            orderBy = json.loads(orderBy)
            query += " ORDER BY " + orderBy['fields']

        if  limit : # Add `Limit` Clause
            limit = json.loads(limit)
            query += " LIMIT " + limit['numbers']

        if  returning :  # Add `Returning` Clause (INSERT ONLY !!!)
            returning = json.loads(returning)
            query += " RETURNING " + returning['fields']


    # Builder (Select, Insert, Update, Delete) and Statment Executions
    # ----------------------------------------------------------------

        if  request.method == 'GET': 
        # --------------------------
            query  = "SELECT * FROM " + tablename + query
            result = conn.execute(text(query)).fetchall()
            result = set(result) if result or len(list(result)) == 0 else abort(HTTPException)

            return result

        elif request.method == 'POST':
        # ----------------------------
            data   = request.form 
            fields = ""
            values = ""

            # Set Fields and Values Data for Insert
            for key, value in data.items(): 
                fields += key + ", " if key != list(data)[-1] else key
                values += "'"+value+"', " if key != list(data)[-1] else "'"+value+"'"

            # Set Insert Clause
            fields = "("+fields+")"
            values = "("+values+")"

            if returning :
                # Execute Query and Set Response Message
                query  = "INSERT INTO "+tablename+" "+fields+" VALUES "+values + query
                result = conn.execute(text(query)).fetchall()
                result = set(result) if result else abort(HTTPException)
                conn.commit()

            else :
                # Execute Query and Set Response Message
                query  = "INSERT INTO "+tablename+" "+fields+" VALUES "+values
                result = conn.execute(text(query))
                result = set({"message" : "Data Successfully Added"}) if result else abort(HTTPException)
                conn.commit()

            return result

        elif request.method == 'PUT':
        # ---------------------------
            data  = request.form
            sets  = ""

            # Set Sets Data for Update
            for key, value in data.items():
                value = 'NULL' if value == "" or value is None else "'" + value + "'"
                sets += key + "=" + value + ", " if key != list(data)[-1] else key + "=" + value

            # Set Update Clause
            sets   = " SET " + sets
            query  = "UPDATE " + tablename + sets + query 

            # Execute Query and Set Response Message
            result = conn.execute(text(query))
            result = set({"message" : "Data Successfully Updated"}) if result else abort(HTTPException)
            conn.commit()

            return result

        elif request.method == 'DELETE':
        # ------------------------------
            # Set Delete Clause
            query = "DELETE FROM " + tablename + query

            result = conn.execute(text(query))
            result = set({"message" : "Data Successfully Deleted"}) if result else abort(HTTPException)
            conn.commit()

            return result

@app.route('/api/2.0/<tablename>', methods=['GET', 'POST', 'PUT', 'PATCH', 'DELETE'])
# @jwt_required()
def base2(tablename):
    '''
        Base API for CRUD Only from One Table.
        @use `pg8000` Python Database Engine for Postgree.
        @method `Query Builders` Where, WhereOr, OrderBy, Limit, Returning.
        @method `GET` `Fetching` Requested Data from Database.
        @method `POST` `Inserting` Data to Database.
        @method `PUT` `Update` Requested Data to Database.
        @method `DELETE` `Delete` Requested Data to Database.
    '''
    try:
        with db.connect() as conn:

        # Additional Clause Builder (From Request Arguments)
        # --------------------------------------------------
            condition     = ""
            where     = request.args.get('where')
            whereOr   = request.args.get('whereOr')
            orderBy   = request.args.get('orderBy')
            limit     = request.args.get('limit')
            returning = request.args.get('returning')

            if where:  # Format `Where` Clause
                where = json.loads(where)
                where_conditions = []
                for key, value in where.items():
                    where_conditions.append(f"{key} {value}")
                condition += " AND ".join(where_conditions)

            if whereOr:  # Format `Where Or` Clause
                whereOr = json.loads(whereOr)
                whereOr_conditions = []
                for key, value in whereOr.items():
                    whereOr_conditions.append(f"{key} {value}")
                if condition != "":  # Ensure WHERE exists before adding OR conditions
                    condition += " AND "
                condition += "(" + " OR ".join(whereOr_conditions) + ")"

            if where or whereOr:  # Add `Where | Where Or` Clause
                condition = " WHERE " + condition

            if  orderBy : # Add `Order By` Clause
                orderBy = json.loads(orderBy)
                condition += " ORDER BY " + orderBy['fields']

            if  limit : # Add `Limit` Clause
                limit = json.loads(limit)
                condition += " LIMIT " + limit['numbers']

            if  returning :  # Add `Returning` Clause (INSERT ONLY !!!)
                returning = json.loads(returning)
                condition += " RETURNING " + returning['fields']


        # Builder (Select, Insert, Update, Delete) and Statment Executions
        # ----------------------------------------------------------------

            if  request.method == 'GET': 
            # --------------------------
                query  = "SELECT * FROM " + tablename + condition
                result = conn.execute(text(query)).fetchall()
                result = set(result)

                return result


            elif request.method == 'POST':
            # ----------------------------
                data   = request.form 
                fields = ""
                values = ""

                # Set Fields and Values Data for Insert
                for key, value in data.items(): 
                    # Hash password if the column is 'password'
                    if key == 'password':
                        hashed_password = bcrypt.generate_password_hash(value).decode('utf-8')
                        value = hashed_password

                    fields += key + ", " if key != list(data)[-1] else key
                    values += "'"+value+"', " if key != list(data)[-1] else "'"+value+"'"

                # Set Insert Clause
                fields = "("+fields+")"
                values = "("+values+")"

                if returning :
                    # Execute Query and Set Response Message
                    query  = "INSERT INTO "+tablename+" "+fields+" VALUES "+values + condition
                    result = conn.execute(text(query)).fetchall()
                    result = set(result)
                    conn.commit()

                else :
                    # Execute Query and Set Response Message
                    query  = "INSERT INTO "+tablename+" "+fields+" VALUES "+values
                    result = conn.execute(text(query))
                    result = set({"message" : "Data berhasil disimpan."})
                    conn.commit()

                return result

            elif request.method == 'PUT' or request.method == 'PATCH':
            # ---------------------------
                data  = request.form
                sets  = ""

                # Set Sets Data for Update
                for key, value in data.items():
                    # Hash password if the column is 'password'
                    if key == 'password':
                        hashed_password = bcrypt.generate_password_hash(value).decode('utf-8')
                        value = hashed_password

                    value = 'NULL' if value == "" or value is None else "'" + value + "'"
                    sets += key + "=" + value + ", " if key != list(data)[-1] else key + "=" + value

                # Set Update Clause
                sets   = " SET " + sets
                query  = "UPDATE " + tablename + sets + condition 

                # Execute Query and Set Response Message
                result = conn.execute(text(query))
                result = set({"message" : "Data berhasil disimpan."})
                conn.commit()

                return result

            elif request.method == 'DELETE':
            # ------------------------------
                # Set Delete Clause
                query = "DELETE FROM " + tablename + query

                result = conn.execute(text(query))
                result = set({"message" : "Data berhasil dihapus."})
                conn.commit()

                return result

    except Exception as e:
        return handle_error(e)


# ---------------------------------------------------------------------------------
# CUSTOM API (EXTRA)
# ---------------------------------------------------------------------------------
id_tem = 0
checkifperbantuan = False

@app.route('/api/extra/getUserToken', methods=['POST'])
def extra1():
    '''
        Mendapatkan Token User berdasarkan Id, Username atau Password untuk Autentikasi.  
        @exception Jangan dikasih `@token_auth`.
        @param `id | username | password` dari User `request.form | form.body`.
        @result `dict` Token User.
    '''

    with db.connect() as conn:

        # id     = request.form.get('id')
        uname  = request.form.get('username')
        paswd  = request.form.get('password')
        uname  = re.sub('[^A-Za-z0-9 -]+', '', uname)
        paswd  = re.sub('[^A-Za-z0-9 -]+', '', paswd)
        query  = ""

        # if id    : # Add Where Id Field
        #     query += "id='" + id + "'" if query == "" else "AND id='" + id + "'"
        # if uname : # Add Username Id Field
            # query += "username='" + uname + "'" if query == "" else "AND username='" + uname + "'"
        # if paswd : # Add Password Id Field
            # query += "password='" + paswd + "'" if query == "" else "AND password='" + paswd + "'"

        # query  = "SELECT token FROM login WHERE " + query
        # result = conn.execute(text(query)).fetchone()
        # result = set({"token" : result["token"]}) if result else abort(HTTPException)
        # result = set(result) if result else abort(HTTPException)
        query  = text("SELECT token FROM karyawan WHERE username=:username")
        query  = query.bindparams(username=uname)
        result = conn.execute(query).fetchone()
        if result :  
            result = set({"token" : result[0]}) if result else abort(HTTPException)
        else :
            return 'Username not found', 404


        return result

@app.route('/api/extra/getUser')
# @token_auth.login_required
def extra2():
    '''
        Mendapatkan Data User berdasarkan Id atau Mendapatkan List User.
        @param `id` dari User `request.argument | query.param` (opsional).
        @result `dict` Data | List Users join Cabang dan Jabatan.
    '''

    with db.connect() as conn:

        id = request.args.get('id')
        # id    = re.sub('[^A-Za-z0-9 -]+','', id)
        query = ""

        if id :
            query = "WHERE karyawan.id='" + id + "' " # Add Where Id Field

        query = f" SELECT karyawan.*,  cabang.nama_cabang AS nama_cabang FROM karyawan LEFT JOIN cabang ON karyawan.id_cabang = cabang.id {query} ORDER BY karyawan.id ".format(query = query)

        result = conn.execute(text(query)).fetchall()
        result = set(result) if result else abort(HTTPException)

        return result


@app.route('/api/extra/login', methods=['POST'])
# # @token_auth.login_required
def extra3():
    '''
        Mendapatkan Data Detail User Setelah Sukses Login.
        @result `dict` List Users join Jabatan dan Cabang.
    '''

    with db.connect() as conn:   

        uname  = request.form.get('username')
        paswd  = request.form.get('password')
        # uname  = re.sub('[^A-Za-z0-9 -]+', '', uname)

        query  = text("""SELECT karyawan.*, cabang.nama_cabang, cabang.nama_resmi, cabang.npwp, jabatan.jabatan 
                         FROM karyawan 
                            LEFT JOIN cabang ON karyawan.id_cabang = cabang.id
                            LEFT JOIN jabatan ON karyawan.id_jabatan = jabatan.id
                         WHERE username=:username""")
        # querycheck  = text("SELECT * FROM user_temp WHERE id_karyawan=:id_karyawan AND tgl_perbantuan =:timeno ")

        hashed_password = bcrypt.generate_password_hash(paswd).decode('utf-8')
        query  = query.bindparams(username=uname)
        result = conn.execute(query).fetchall()

        # # Token
        # # SQL query to fetch permissions based on karyawan ID
        # query_permission  = text("""
        #     SELECT f.nama, f.permission
        #     FROM fitur f
        #     JOIN hakakses h ON f.id = h.id_fitur
        #     WHERE h.id_karyawan = :id_karyawan;
        # """)
        # query_permission  = query_permission.bindparams(id_karyawan=result[0][0])
        # result_permission = conn.execute(query_permission).fetchall()

        # # Structure the permissions dictionary
        # permissions = {}
        # for row in result_permission:
        #     fitur = row[0]
        #     permission = row[1]

        #     if fitur not in permissions:
        #         permissions[fitur] = []
        #     permissions[fitur].append(permission)

        # Creat token
        # access_token = create_access_token(identity=permissions) 
        access_token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmcmVzaCI6ZmFsc2UsImlhdCI6MTczMDM0MTY4OSwianRpIjoiMTE5MjUwYjktMGE2Ny00OGUzLWFiMmEtNmFjNjg4ZWZkZjU0IiwidHlwZSI6ImFjY2VzcyIsInN1YiI6eyJudWxsIjpbbnVsbF0sImFudHJpYW5fbm9tb3JfbWVqYSI6WyJjcmVhdGUiLCJ1cGRhdGUiLCJ2aWV3Il19LCJuYmYiOjE3MzAzNDE2ODksImV4cCI6MTczMDQyODA4OX0.D3cWyhQy_BVj2wO751TEFkXRdv4ztymv9Vqy6xBnzlg"

        # check if karyawan perbantuan
        # querycheck  = querycheck.bindparams(id_karyawan=result[0][0], timeno = changetypedate(timeZoneDate()) )
        # resultcheck = conn.execute(querycheck).fetchall()
        if result:
            if result[0][1] and bcrypt.check_password_hash(result[0][2], paswd):
                return {"token": access_token, "result": setLogin(result)}, 200
            else :
                return "Password Dont Match", 401
        # if resultcheck : 
        #     global checkifperbantuan
        #     global id_tem
        #     checkifperbantuan = True
        #     id_tem = resultcheck[0][0]
        #     if result[0][1] and bcrypt.check_password_hash(result[0][2], paswd):
        #         return {"token": access_token, "result": setLogin(result)}, 200
        #     elif result[0][15] :
        #         hashed_user_password = hashlib.md5(paswd.encode()).hexdigest()
        #         if hashed_user_password == result[0][12]:
        #             hashed_password = bcrypt.generate_password_hash(paswd).decode('utf-8')
        #             addintodb  = text("UPDATE karyawan SET password_baru = :password_baru, password_lama = NULL  WHERE username = :username")
        #             query  = addintodb.bindparams(password_baru=hashed_password, username=uname)        
        #             conn.execute(query)
        #             conn.commit() 
        #             return {"token": access_token, "result": setLogin(result)}, 200
        #         else :
        #             return "Password Dont Match", 401
        #     else :
        #         return "Password Dont Match", 401
        # else :            
        #     checkifperbantuan = False
        #     if result[0][1] and bcrypt.check_password_hash(result[0][2], paswd):
        #         return {"token": access_token, "result": setLogin(result)}, 200
        #     elif result[0][13] :
        #         hashed_user_password = hashlib.md5(paswd.encode()).hexdigest()
        #         if hashed_user_password == result[0][15]:
        #             print("Find Password Lama")
        #             hashed_password = bcrypt.generate_password_hash(paswd).decode('utf-8')
        #             addintodb  = text("UPDATE karyawan SET password_baru = :password_baru, password_lama = NULL  WHERE username = :username")
        #             query  = addintodb.bindparams(password_baru=hashed_password, username=uname)        
        #             conn.execute(query)
        #             conn.commit()  
        #             return {"token": access_token, "result": setLogin(result)}, 200
        #         else :
        #             return "Password Dont Match", 401
        else :
            return "Password Dont Match", 401

@app.route('/api/extra/getFiturUser')
# @token_auth.login_required
def extra4():
    '''
        Mendapatkan List Fitur berdasarkan Id User.
        @result `dict` List Fitur join Users Akses.
    '''

    with db.connect() as conn:

        id    = request.args.get('id')
        id    = re.sub('[^A-Za-z0-9 -]+', '', id)


        query = text("""
        SELECT hakakses.*, 
            fitur.fitur,
           
            karyawan.username, karyawan.hp, karyawan.nik,
            cabang.nama_cabang as cabang
        FROM hakakses 
            LEFT JOIN fitur ON hakakses.id_fitur = fitur.id
            LEFT JOIN karyawan ON hakakses.id_karyawan = karyawan.id
            LEFT JOIN cabang ON karyawan.id_cabang = cabang.id
        WHERE hakakses.id_karyawan = :id
        ORDER BY hakakses.id
    """)

        query  = query.bindparams(id=int(id))
        result = conn.execute(query).fetchall()
        result = set(result) if result or len(list(result)) == 0 else abort(HTTPException)

        return result

@app.route('/api/extra/getFiturUserTemp')
# @token_auth.login_required
def extra5():
    '''
        Mendapatkan List Fitur berdasarkan Id User.
        @result `dict` List Fitur join Users Akses.
    '''

    with db.connect() as conn:

        id    = request.args.get('id')
        id    = re.sub('[^A-Za-z0-9 -]+', '', id)

        query = text("""
            SELECT hakakses_temp .*, 
                fitur.fitur AS nama_fitur,             
                karyawan.username, karyawan.hp, karyawan.nik,
                cabang.nama_cabang as cabang
            FROM hakakses_temp 
                LEFT JOIN fitur ON hakakses_temp.id_fitur = fitur.id
                LEFT JOIN karyawan ON hakakses_temp.id_karyawan = karyawan.id
                LEFT JOIN cabang ON karyawan.id_cabang = cabang.id
            WHERE hakakses_temp.id_karyawan = :id
            ORDER BY hakakses_temp.id
        """)


        query  = query.bindparams(id=int(id))
        result = conn.execute(query).fetchall()
        result = set(result) if result or len(list(result)) == 0 else abort(HTTPException)

        return result


@app.route('/api/extra/getAllFiturUser')
# @token_auth.login_required
def extra6():
    '''
        Mendapatkan List Fitur yang dapat dipilih berdasarkan Id User .
        @result `dict` List Fitur Not Exist Id User di User Akses.
    '''


    with db.connect() as conn:

        id    = request.args.get('id')
        id    = re.sub('[^A-Za-z0-9 -]+', '', id)    
        query = text("""
            SELECT fitur.id, fitur.kategori_karyawan, fitur.fitur as text FROM fitur
            WHERE NOT EXISTS (
                SELECT id_fitur 
                FROM hakakses
                WHERE id_karyawan = :id
                AND fitur.id = hakakses.id_fitur
                )
                ORDER BY fitur;
            """)

        query  = query.bindparams(id=int(id))
        result = conn.execute(query).fetchall()
        result = set(result) if result or len(list(result)) == 0 else abort(HTTPException)

        return result

@app.route('/api/extra/getAllFiturUserTemp')
# @token_auth.login_required
def extra7():
    '''
        Mendapatkan List Fitur yang dapat dipilih berdasarkan Id User .
        @result `dict` List Fitur Not Exist Id User di User Akses.
    '''


    with db.connect() as conn:

        id    = request.args.get('id')
        id    = re.sub('[^A-Za-z0-9 -]+', '', id)

        query = text("""
                SELECT fitur.id, fitur.fitur as text FROM fitur
                WHERE NOT EXISTS (
                    SELECT id_fitur 
                    FROM hakakses_temp
                    WHERE id_karyawan = :id
                    AND fitur.id = hakakses_temp.id_fitur
                )
                ORDER BY fitur;
            """)

        query  = query.bindparams(id=int(id))
        result = conn.execute(query).fetchall()
        result = set(result) if result or len(list(result)) == 0 else abort(HTTPException)

        return result


@app.route('/api/extra/getDetailKaryawan')
# @token_auth.login_required
def extra8():

    '''
        Mendapatkan Detail Karyawan.
        @result `dict` Detail Karyawan.
    '''

    with db.connect() as conn:

        id    = request.args.get('id')
        query = text("""
            SELECT karyawan.*,
                cabang.nama_cabang as cabang
            FROM karyawan 
                LEFT JOIN cabang ON karyawan.id_cabang = cabang.id
            WHERE karyawan.id = :id
        """)

        query  = query.bindparams(id=int(id))
        result = conn.execute(query).fetchall()
        result = set(result) if result or len(list(result)) == 0 else abort(HTTPException)

        return result


@app.route('/api/extra/getProduk')
# @token_auth.login_required
def extra9():
    '''
        Mendapatkan List Produk.
        @result `dict` List Produk.
    '''

    with db.connect() as conn:

        id    = request.args.get('id')
        query = text("""
            SELECT produk.*,
                jenis.jenis,
				kategori.kategori,
                cabang.nama as cabang
            FROM produk 
                LEFT JOIN jenis ON produk.id_jenis = jenis.id
                LEFT JOIN kategori ON produk.id_kategori = kategori.id
                LEFT JOIN cabang ON produk.id_cabang = cabang.id
            WHERE produk.id_cabang = :id
        """)

        query  = query.bindparams(id=int(id))
        result = conn.execute(query).fetchall()
        result = set(result) if result or len(list(result)) == 0 else abort(HTTPException)

        return result


@app.route('/api/extra/upload', methods=['POST'])
# @token_auth.login_required
def extra10():
    '''
        untuk upload data.
        @result `dict` nama file bucket.
    '''
    with db.connect() as conn:

        # original = request.files.get('file', None)
        # # original = request.files['file']

        # storage_client = storage.Client('jaya.json')
        # bucket = storage_client.bucket('jaya-pic')
        # blob = bucket.blob(randomword(16))

        # # result = set(result) if result else abort(HTTPException)

        # generation_match_precondition = 0

        # blob.upload_from_filename(original.filename, if_generation_match=generation_match_precondition)

        # print(
        #     f"File {original.filename} uploaded to {randomword(16)}."
        # )

        file = request.files['file']
        # If the user does not select a file, the browser submits an
        # empty file without a filename.
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            # filename = secure_filename(file.filename)
            filename = file.filename
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            # return redirect(url_for('download_file', name=filename))

            credential_path = "larissa-storage.json"
            os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = credential_path

            storage_client = storage.Client('larissa-storage.json')
            bucket = storage_client.bucket('larissa-aset')
            randomname = randomword(16)
            os.rename('uploads/'+file.filename, 'uploads/'+randomname)
            blob = bucket.blob(randomname)

            # result = set(result) if result else abort(HTTPException)

            generation_match_precondition = 0

            blob.upload_from_filename('uploads/'+randomname, if_generation_match=generation_match_precondition)
            blob.make_public()
            os.remove('uploads/'+randomname)
            # return "File Sucessfully Uploaded"

            result = set({"image" : randomname})

            return result

@app.route('/api/extra/addKaryawan', methods=['POST'])
# # @token_auth.login_required
def extra11():
    '''
        Mendapatkan Data Detail User Setelah Sukses Login.
        @result `dict` Tambah Data Karyawan.
    '''

    with db.connect() as conn:

        username  = request.form.get('username')
        tgllahir  = dt.strptime(request.form.get('tgllahir'), '%Y-%m-%d').date()
        jenis_kelamin  = request.form.get('jenis_kelamin')
        email  = request.form.get('email')
        hp   = request.form.get('hp')
        nik    = request.form.get('nik')
        tgl_buat = dt.strptime(request.form.get('tgl_buat'), '%Y-%m-%d').date()
        foto   = request.form.get('foto')
        id_jabatan   = request.form.get('id_jabatan')
        id_cabang   = request.form.get('id_cabang')
        id_fitur   = request.form.get('id_fitur')
        tgl_exp   = dt.strptime(request.form.get('tgl_exp'), '%Y-%m-%d').date()
        status   = request.form.get('status')
        alamat_dom   = request.form.get('alamat_dom')
        alamat_ktp   = request.form.get('alamat_ktp')
        id_provinsi_ktp   = request.form.get('id_provinsi_ktp')
        id_kabupaten_ktp   = request.form.get('id_kabupaten_ktp')
        id_kecamatan_ktp   = request.form.get('id_kecamatan_ktp')
        id_kelurahan_ktp   = request.form.get('id_kelurahan_ktp')
        id_provinsi_dom   = request.form.get('id_provinsi_dom')
        id_kabupaten_dom   = request.form.get('id_kabupaten_dom')
        id_kecamatan_dom   = request.form.get('id_kecamatan_dom')
        id_kelurahan_dom   = request.form.get('id_kelurahan_dom')
        password_baru   = request.form.get('password_baru')
        kategorikaryawan = request.form.get('kategorikaryawan')
        nomor_induk = request.form.get("nomor_induk")
        sip_dokter = request.form.get("sip_dokter")
        nama_karyawan = request.form.get("nama_karyawan")

        # tglbuat= timeZoneDate()
        token  = randomword(32)

        # username  = re.sub('[^A-Za-z0-9 -]+', '', username)
        # password_baru  = re.sub('[^A-Za-z0-9 -]+', '', password_baru)

        query  = ""

        checkusername = text("SELECT * FROM karyawan WHERE username=:username")
        checkquery = checkusername.bindparams(username=username)
        getresultcheck = conn.execute(checkquery).fetchone()

        if len(nik) != 16:
            return jsonify(message="NIK Harus 16 Digit"), 400


        hashed_password = bcrypt.generate_password_hash(password_baru).decode('utf-8')
        query  = text("""INSERT INTO karyawan (username, tgllahir, jenis_kelamin, email, hp, 
                            nik, tgl_buat,  foto, id_cabang, tgl_exp, status, alamat_dom, 
                            alamat_ktp, id_provinsi_ktp, id_kabupaten_ktp, id_kecamatan_ktp, 
                            id_kelurahan_ktp, id_provinsi_dom, id_kabupaten_dom, id_kecamatan_dom, 
                            id_kelurahan_dom, password_baru, token, kategorikaryawan, id_jabatan, nomor_induk,
                            sip_dokter, nama_karyawan) 
                        VALUES (:username, :tgllahir, :jenis_kelamin, :email, :hp, :nik,
                        :tgl_buat ,:foto, :id_cabang, :tgl_exp, :status, :alamat_dom, 
                        :alamat_ktp, :id_provinsi_ktp, :id_kabupaten_ktp, :id_kecamatan_ktp, 
                        :id_kelurahan_ktp, :id_provinsi_dom, :id_kabupaten_dom, :id_kecamatan_dom, 
                        :id_kelurahan_dom, :password_baru, :token, :kategorikaryawan, :id_jabatan, :nomor_induk,
                        :sip_dokter, :nama_karyawan)""")
        query  = query.bindparams(username=username, tgllahir=tgllahir, jenis_kelamin=jenis_kelamin, email=email,
                                hp=hp, nik=nik, tgl_buat=tgl_buat, foto=foto, 
                                id_cabang=int(id_cabang), tgl_exp=tgl_exp, 
                                status=int(status), alamat_dom=alamat_dom, alamat_ktp=alamat_ktp, 
                                id_provinsi_ktp=int(id_provinsi_ktp), 
                                id_kabupaten_ktp=int(id_kabupaten_ktp),
                                id_kecamatan_ktp=int(id_kecamatan_ktp), 
                                id_kelurahan_ktp=int(id_kelurahan_ktp), 
                                id_provinsi_dom=int(id_provinsi_dom),
                                id_kabupaten_dom=int(id_kabupaten_dom), 
                                id_kecamatan_dom=int(id_kecamatan_dom),
                                id_kelurahan_dom=int(id_kelurahan_dom),
                                password_baru=hashed_password, token=token, 
                                kategorikaryawan=kategorikaryawan, id_jabatan=int(id_jabatan), nomor_induk=nomor_induk,
                                sip_dokter=sip_dokter, nama_karyawan=nama_karyawan)
        result1 = conn.execute(query)      
        conn.commit()   
        getuser = text("SELECT id FROM karyawan WHERE username=:username")
        querycheck  = getuser.bindparams(username=username)
        resultcheck = conn.execute(querycheck).fetchone()
        addjab_utama = text("INSERT INTO jabatan_utama (id_karyawan, id_jabatan) VALUES (:id_karyawan, :id_jabatan)")
        addjab  = addjab_utama.bindparams(id_karyawan=int(resultcheck[0]), id_jabatan=int(id_jabatan) )
        conn.execute(addjab)      
        conn.commit() 
        add_fitur = text("INSERT INTO hakakses (id_karyawan, id_fitur) VALUES (:id_karyawan, :id_fitur)")
        addfit  = add_fitur.bindparams(id_karyawan=int(resultcheck[0]), id_fitur=int(id_fitur) )
        conn.execute(addfit)      
        conn.commit() 
        result1 = set({"message" : "Data Successfully Added", "status" : " 200"}) if result1 else abort(HTTPException)
        return result1    


@app.route('/api/extra/editpasword', methods=['PUT'])
# @token_auth.login_required
def extra12():
    '''
        Mendapatkan Data Detail User Setelah Sukses Login.
        @result `dict` Tambah Data Karyawan.
    '''

    with db.connect() as conn:


        id    = request.args.get('id')
        password_baru   = request.form.get('password_baru')      
        hashed_password = bcrypt.generate_password_hash(password_baru).decode('utf-8')     
        query  = text("UPDATE karyawan SET password_baru=:password_baru WHERE id=:id")
        query  = query.bindparams(password_baru=hashed_password, id=int(id))
        result1 = conn.execute(query)      
        conn.commit()   

        result1 = set({"message" : "Data Successfully Update", "status" : " 200"}) if result1 else abort(HTTPException)
        return result1    

@app.route('/api/extra/getallcabang', methods=['GET'])
# @token_auth.login_required
def extra13():
    '''
        Mendapatkan Data Detail User Setelah Sukses Login.
        @result `dict` Tambah Data Karyawan.
    '''

    with db.connect() as conn:

        query  = text("SELECT cabang.id, cabang.nama_cabang as text FROM CABANG")

        result = conn.execute(query).fetchall()
        result = set(result) if result else abort(HTTPException)
        return result



@app.route('/api/extra/getDropJabatan')
# @token_auth.login_required
def extra14():
    '''
        Mendapatkan List Jabatan All.
        @result `dict` List Jabatan untuk Dropdown.
    '''

    with db.connect() as conn:

        query = text("""
            SELECT id, jabatan as text
            FROM jabatan 
            ORDER BY jabatan ASC
        """)

        result = conn.execute(query).fetchall()
        result = set(result) if result else abort(HTTPException)

        return result

@app.route('/api/extra/getDropCabang')
# @token_auth.login_required
def extra15():
    '''
        Mendapatkan List Cabang All.
        @result `dict` List Cabang untuk Dropdown.
    '''

    with db.connect() as conn:

        query = text("""
            SELECT id, nama_cabang as text
            FROM cabang 
            ORDER BY cabang ASC
        """)

        result = conn.execute(query).fetchall()
        result = set(result) if result else abort(HTTPException)

        return result



@app.route('/api/extra/getListKaryawan')
# @token_auth.login_required
def extra16():
    '''
        Mendapatkan List Karyawan All.
        @result `dict` List karyawan all.
    '''

    with db.connect() as conn:

        query = """
            SELECT * 
            FROM karyawan AS l
                LEFT JOIN cabang AS c ON l.id_cabang = c.id
            ORDER BY l.username ASC
        """
        # query = """
        #     SELECT l.id, l.username, l.status, l.hp, l.email,  l.nik, 
        #       c.nama_cabang AS cabang
        #     FROM karyawan AS l
        #         LEFT JOIN cabang AS c ON l.id_cabang = c.id
        #     ORDER BY l.username ASC
        # """

        result = conn.execute(text(query)).fetchall()
        result = set(result) if result else abort(HTTPException)

        return result

@app.route('/api/extra/getListJenisProduk')
# @token_auth.login_required
def extra17():
    '''
        Mendapatkan List Jenis-jenis Produk.
        @result `dict` List Jenis-jenis Produk.
    '''

    with db.connect() as conn:

        query = text("""
            SELECT id, jenis as text FROM jenis
            ORDER BY jenis;
        """)

        result = conn.execute(query).fetchall()
        result = set(result) if result or len(list(result)) == 0 else abort(HTTPException)

        return result

@app.route('/api/extra/getListKategoriProduk')
# @token_auth.login_required
def extra18():
    '''
        Mendapatkan List Kategori Produk.
        @result `dict` List Kategori Produk.
    '''

    with db.connect() as conn:

        query = text("""
            SELECT id, nama_kategori as text FROM kategori
            ORDER BY kategori;
        """)

        result = conn.execute(query).fetchall()
        result = set(result) if result or len(list(result)) == 0 else abort(HTTPException)

        return result

# get list aktivitas

@app.route('/api/extra/gruptext')
# @token_auth.login_required
def extra19():
    '''
        Mendapatkan List Aktivitas.
        @result `dict` List Aktivitas.
    '''

    with db.connect() as conn:

        query = text("""
        select id, name_grupaset as text
       FROM grup_aset 
         ORDER BY grup_aset ASC
        """)

        result = conn.execute(query).fetchall()
        result = set(result) if result or len(list(result)) == 0 else abort(HTTPException)

        return result

@app.route('/api/extra/getlistaktivitas')
# @token_auth.login_required
def extra20():
    '''
        Mendapatkan List Aktivitas.
        @result `dict` List Aktivitas.
    '''

    with db.connect() as conn:

        query = text("""
        select a.id, a.nama_aktivitas, a.keterangan, c.name_grupaset 
        from aktivitas as a
        left join grup_aset as c on a.id_grup = c.id
        order by a.nama_aktivitas ASC
        """)

        result = conn.execute(query).fetchall()
        result = set(result) if result or len(list(result)) == 0 else abort(HTTPException)

        return result


    # cabang section

@app.route('/api/extra/getkelurahan')
# @token_auth.login_required
def extra21():
    '''
        Mendapatkan List kelurahan.
        @result `dict` List kelurahan.
    '''

    with db.connect() as conn:

        query = text("""
        select id, kode_kelurahan, keterangan, nama_kelurahan as text
        from kelurahan 
        order by nama_kelurahan ASC
        """)

        result = conn.execute(query).fetchall()
        result = set(result) if result or len(list(result)) == 0 else abort(HTTPException)

        return result


@app.route('/api/extra/getkecamatan')
# @token_auth.login_required
def extra22():
    '''
        Mendapatkan List kecamatan.
        @result `dict` List kecamatan.
    '''

    with db.connect() as conn:

        query = text("""
        select id, kode_kecamatan, keterangan, nama_kecamatan as text
        from kecamatan 
        order by nama_kecamatan ASC
        """)

        result = conn.execute(query).fetchall()
        result = set(result) if result or len(list(result)) == 0 else abort(HTTPException)

        return result


@app.route('/api/extra/getkabupaten')
# @token_auth.login_required
def extra23():
    '''
        Mendapatkan List kabupaten.
        @result `dict` List kabupaten.
    '''

    with db.connect() as conn:

        query = text("""
        select id, kode_kabupaten, keterangan, nama_kabupaten as text
        from kabupaten 
        order by nama_kabupaten ASC
        """)

        result = conn.execute(query).fetchall()
        result = set(result) if result or len(list(result)) == 0 else abort(HTTPException)

        return result


@app.route('/api/extra/getprovinsi')
# @token_auth.login_required
def extra24():
    '''
        Mendapatkan List provinsi.
        @result `dict` List provinsi.
    '''

    with db.connect() as conn:

        query = text("""
        select id, kode_provinsi, keterangan, nama_provinsi as text
        from provinsi 
        order by nama_provinsi ASC
        """)

        result = conn.execute(query).fetchall()
        result = set(result) if result or len(list(result)) == 0 else abort(HTTPException)

        return result


@app.route('/api/extra/getwilayah')
# @token_auth.login_required
def extra25():
    '''
        Mendapatkan List wilayah.
        @result `dict` List wilayah.
    '''

    with db.connect() as conn:

        query = text("""
        select id, kode_wilayah, keterangan, nama_wilayah as text
        from wilayah 
        order by nama_wilayah ASC
        """)

        result = conn.execute(query).fetchall()
        result = set(result) if result or len(list(result)) == 0 else abort(HTTPException)

        return result



@app.route('/api/extra/getdetailusertemp')
# @token_auth.login_required
def extra26():
    '''
        Mendapatkan List wilayah.
        @result `dict` List wilayah.
    '''

    with db.connect() as conn:

        id    = request.args.get('id')
        id    = re.sub('[^A-Za-z0-9 -]+', '', id)

        query = text("""
        SELECT  karyawan.id, karyawan.username, karyawan.id_cabang,
                d.nama_cabang,
                c.nama_cabang as cabangperbantuan,  
                jabatan.jabatan,         
                user_temp.*              
                
        FROM user_temp 
                LEFT JOIN karyawan ON user_temp.id_karyawan = karyawan.id
                LEFT JOIN cabang as d ON karyawan.id_cabang = d.id
                LEFT JOIN cabang as c ON user_temp.id_cabang_baru = c.id    
                LEFT JOIN jabatan ON user_temp.jabatan_temp= jabatan.id
        
        where user_temp.id_karyawan = :id
        """)

        query  = query.bindparams(id=int(id))
        result = conn.execute(query).fetchall()
        result = set(result) if result or len(list(result)) == 0 else abort(HTTPException)

        return result

@app.route('/api/extra/getfitur')
# @token_auth.login_required
def extra27():
    '''
        Mendapatkan List wilayah.
        @result `dict` List wilayah.
    '''

    with db.connect() as conn:


        query = text("""
        SELECT fitur.id, fitur.jabatan , fitur.kategori_karyawan, fitur.fitur as text               
        FROM fitur              
               """)


        result = conn.execute(query).fetchall()
        result = set(result) if result or len(list(result)) == 0 else abort(HTTPException)

        return result

@app.route('/api/extra/getgrupaset')
# @token_auth.login_required
def extra28():
    '''
        Mendapatkan List wilayah.
        @result `dict` List wilayah.
    '''

    with db.connect() as conn:


        query = text("""
        SELECT grup_aset.id, grup_aset.id_kelas,grup_aset.name_grupaset as text               
        FROM grup_aset              
               """)       

        result = conn.execute(query).fetchall()
        result = set(result) if result or len(list(result)) == 0 else abort(HTTPException)

        return result

@app.route('/api/extra/getallaset')
# @token_auth.login_required
def extra29():
    '''
        Mendapatkan List wilayah.
        @result `dict` List wilayah.
    '''

    with db.connect() as conn:


        query = text("""
        SELECT aset.*, cabang.nama_cabang, kategori.nama_kategori
        FROM aset            
                LEFT JOIN cabang ON aset.id_cabang = cabang.id  
                LEFT JOIN kategori ON aset.id_kategori = kategori.id  
               """)      


        result = conn.execute(query).fetchall()
        result = set(result) if result or len(list(result)) == 0 else abort(HTTPException)

        return result



@app.route('/api/extra/getaktivitasaset')
# @token_auth.login_required
def extra30():
    '''
        Mendapatkan List wilayah.
        @result `dict` List wilayah.
    '''

    with db.connect() as conn:

        id = request.args.get('id')     
        query = text("""
        SELECT aktivitas.id, nama_aktivitas as text
        FROM aktivitas
        WHERE aktivitas.id_grup = :id
               """)      

        query  = query.bindparams(id=int(id))
        result = conn.execute(query).fetchall()
        result = set(result) if result or len(list(result)) == 0 else abort(HTTPException)

        return result

@app.route('/api/extra/allaktivitasaset')
# @token_auth.login_required
def extra31():
    '''
        Mendapatkan List wilayah.
        @result `dict` List wilayah.
    '''

    with db.connect() as conn:

        id = request.args.get('id')  

        query = text("""
        SELECT aktivitas_aset.*, aktivitas.nama_aktivitas, aset.nama_awal
        FROM aktivitas_aset
                LEFT JOIN aktivitas on aktivitas_aset.id_aktivitas = aktivitas.id
                LEFT JOIN aset on aktivitas_aset.id_aset = aset.id
                            
        where aktivitas_aset.id_aset = :id
               """)      

        query  = query.bindparams(id=int(id))
        result = conn.execute(query).fetchall()
        result = set(result) if result or len(list(result)) == 0 else abort(HTTPException)

        return result


@app.route('/api/extra/getoneaktivitasaset')
# @token_auth.login_required
def extra32():
    '''
        Mendapatkan List wilayah.
        @result `dict` List wilayah.
    '''

    with db.connect() as conn:

        id = request.args.get('id')  

        query = text("""
        SELECT aktivitas_aset.*, aktivitas.nama_aktivitas, aset.nama_awal
        FROM aktivitas_aset
                LEFT JOIN aktivitas on aktivitas_aset.id_aktivitas = aktivitas.id
                LEFT JOIN aset on aktivitas_aset.id_aset = aset.id
                            
        where aktivitas_aset.id = :id
               """)      

        query  = query.bindparams(id=int(id))
        result = conn.execute(query).fetchall()
        result = set(result) if result or len(list(result)) == 0 else abort(HTTPException)

        return result

@app.route('/api/extra/getAsetRefCabang')
# @token_auth.login_required
def extra33():
    '''
        Mendapatkan List wilayah.
        @result `dict` List wilayah.
    '''

    with db.connect() as conn:

        id = request.args.get('id')  

        query = text("""
        SELECT aset.id, aset.nama_awal as text
        FROM aset
        where aset.id_cabang = :id
               """)      

        query  = query.bindparams(id=int(id))
        result = conn.execute(query).fetchall()
        result = set(result) if result or len(list(result)) == 0 else abort(HTTPException)

        return result

@app.route('/api/extra/getAktivitasAsetRefAset')
# @token_auth.login_required
def extra34():
    '''
        Mendapatkan List wilayah.
        @result `dict` List wilayah.
    '''

    with db.connect() as conn:

        id = request.args.get('id')  

        query = text("""
        SELECT aktivitas_aset.id, aktivitas.nama_aktivitas as text
        FROM aktivitas_aset
            LEFT JOIN aktivitas ON aktivitas_aset.id_aktivitas = aktivitas.id
        where aktivitas_aset.id_aset = :id
               """)      

        query  = query.bindparams(id=int(id))
        result = conn.execute(query).fetchall()
        result = set(result) if result or len(list(result)) == 0 else abort(HTTPException)

        return result

@app.route('/api/extra/getalltask')
# @token_auth.login_required
def extra35():
    '''
        Mendapatkan List wilayah.
        @result `dict` List wilayah.
    '''

    with db.connect() as conn:


        query = text("""
        SELECT aktivitas.nama_aktivitas, task.waktu_sampai as expiry_date,
                aktivitas_aset.status_aktivasi,aset.nama_awal, aset.id_cabang ,aset.id, task.id as id_task, task.status
                    
        FROM task
            LEFT JOIN aktivitas_aset ON task.id_aktivitasaset = aktivitas_aset.id
            LEFT JOIN aktivitas ON aktivitas_aset.id_aktivitas = aktivitas.id
            LEFT JOIN aset ON aktivitas_aset.id_aset = aset.id
            
               """)      


        result = conn.execute(query).fetchall()
        result = set(result) if result or len(list(result)) == 0 else abort(HTTPException)

        return result

@app.route('/api/extra/getDetailTask')
# @token_auth.login_required
def extra36():
    '''
        Mendapatkan List wilayah.
        @result `dict` List wilayah.
    '''

    with db.connect() as conn:

        id = request.args.get('id')  
        query = text("""
        SELECT cabang.nama_cabang, aset.nama_awal, aset.lokasi ,aset.tgl_akuisisi,
                jabatan.jabatan, aktivitas.nama_aktivitas, aktivitas.id as id_aktivitas, task.*, kategori.nama_kategori
                    
        FROM task
            LEFT JOIN aktivitas_aset ON task.id_aktivitasaset = aktivitas_aset.id
            LEFT JOIN aktivitas ON aktivitas_aset.id_aktivitas = aktivitas.id
            LEFT JOIN jabatan ON aktivitas.id_jabatan = jabatan.id
            LEFT JOIN aset ON aktivitas_aset.id_aset = aset.id
            LEFT JOIN kategori ON aset.id_kategori = kategori.id
            LEFT JOIN cabang ON aset.id_cabang = cabang.id
            
        where task.id = :id
               """)      
        query  = query.bindparams(id=int(id))
        result = conn.execute(query).fetchall()
        result = set(result) if result or len(list(result)) == 0 else abort(HTTPException)

        return result


@app.route('/api/extra/getFiturSide')
# @token_auth.login_required
def extra37():
    '''
        Mendapatkan List Fitur berdasarkan Id User.
        @result `dict` List Fitur join Users Akses.
    '''

    with db.connect() as conn:

        id    = request.args.get('id')
        id    = re.sub('[^A-Za-z0-9 -]+', '', id)

        global checkifperbantuan
        global id_tem
        if checkifperbantuan :
            query = text("""
                SELECT hakakses_temp.*, 
                    fitur.fitur
                FROM hakakses_temp 
                    LEFT JOIN fitur ON hakakses_temp.id_fitur = fitur.id
                WHERE hakakses_temp.id_usertemp = :resultcheck
              
                """)
            query  = query.bindparams(resultcheck=int(id_tem))

        else :

            query = text("""
                SELECT hakakses.*, 
                    fitur.fitur,
                    karyawan.username, karyawan.hp, karyawan.nik,
                    cabang.nama_cabang as cabang
                FROM hakakses 
                    LEFT JOIN fitur ON hakakses.id_fitur = fitur.id
                    LEFT JOIN karyawan ON hakakses.id_karyawan = karyawan.id
                    LEFT JOIN cabang ON karyawan.id_cabang = cabang.id
                WHERE hakakses.id_karyawan = :id
                ORDER BY hakakses.id
                """)
            query  = query.bindparams(id=int(id))
        result = conn.execute(query).fetchall()
        result = set(result) if result or len(list(result)) == 0 else abort(HTTPException)

        return result



@app.route('/api/extra/addaset', methods=['POST'])
# @token_auth.login_required
def extra38():
    '''
        Mendapatkan Data Detail User Setelah Sukses Login.
        @result `dict` Tambah Data Karyawan.
    '''

    with db.connect() as conn:

        id_kategori  = request.form.get('id_kategori')
        nama_awal  = request.form.get('nama_awal')
        value_awal  = request.form.get('value_awal')
        depresiasi  = request.form.get('depresiasi')
        value_akhir   = request.form.get('value_akhir')
        tgl_expired    = request.form.get('tgl_expired')
        tgl_akuisisi = request.form.get('tgl_akuisisi')
        status_aset   = request.form.get('status_aset')
        id_cabang   = request.form.get('id_cabang')
        barcode   = request.form.get('barcode')
        keterangan   = request.form.get('keterangan')
        id_grupaset   = request.form.get('id_grupaset')
        lokasi   = request.form.get('lokasi')
        foto   = request.form.get('image')

        query  = ""   
        query  = text("INSERT INTO aset (id_kategori, nama_awal, value_awal, depresiasi, value_akhir, tgl_expired, tgl_akuisisi, status_aset, id_cabang,  barcode, keterangan, id_grupaset, lokasi) VALUES (:id_kategori, :nama_awal, :value_awal, :depresiasi, :value_akhir, '"+tgl_expired+"' ,'"+tgl_akuisisi+"', :status_aset, :id_cabang, :barcode, :keterangan, :id_grupaset, :lokasi )")
        query  = query.bindparams(id_kategori=int(id_kategori), nama_awal=nama_awal, value_awal=int(value_awal), depresiasi=int(depresiasi), value_akhir=int(value_akhir), status_aset=int(status_aset), id_cabang=int(id_cabang), barcode=barcode, keterangan=keterangan,  id_grupaset=int(id_grupaset), lokasi=lokasi)
        result1 = conn.execute(query)      
        conn.commit()   
        getaset = text("SELECT id FROM aset WHERE nama_awal=:nama_awal")
        querycheck  = getaset.bindparams(nama_awal=nama_awal)
        resultcheck = conn.execute(querycheck).fetchone()
        add_detailaset = text("INSERT INTO detail_aset (id_aset, foto) VALUES (:id_aset, :foto)")
        add_det  = add_detailaset.bindparams(id_aset=int(resultcheck[0]), foto=foto )
        conn.execute(add_det)      
        conn.commit() 

        result1 = set({"message" : "Data Successfully Added", "status" : " 200"}) if result1 else abort(HTTPException)
        return result1    

@app.route('/api/extra/getOneAset')
# @token_auth.login_required
def extra39():
    '''
        Mendapatkan List Fitur berdasarkan Id User.
        @result `dict` List Fitur join Users Akses.
    '''

    with db.connect() as conn:

        id    = request.args.get('id')
        id    = re.sub('[^A-Za-z0-9 -]+', '', id)

        global checkifperbantuan

        query = text("""
        SELECT aset.*, detail_aset.foto
           
        FROM aset 
            LEFT JOIN detail_aset ON aset.id = detail_aset.id_aset
        WHERE aset.id = :id
        
        """)

        query  = query.bindparams(id=int(id))
        result = conn.execute(query).fetchall()
        result = set(result) if result or len(list(result)) == 0 else abort(HTTPException)

        return result



@app.route('/api/extra/addperbantuan', methods=['POST'])
# @token_auth.login_required
def extra40():
    '''
        Mendapatkan List Fitur berdasarkan Id User.
        @result `dict` List Fitur join Users Akses.
    '''

    with db.connect() as conn:

        id_karyawan  = request.form.get('id_karyawan')  
        id_cabang_baru  = request.form.get('id_cabang_baru')
        tgl_perbantuan_str  = request.form.get('tgl_perbantuan')
        jabatan_temp  = request.form.get('jabatan_temp')
        FiturPilihan  = request.form.get('FiturPilihan')        

        query  = ""   
        query2  = ""   

        checkDate =  text("SELECT tgl_perbantuan FROM user_temp WHERE id_karyawan=:id_karyawan  ")
        querycheck  = checkDate.bindparams(id_karyawan=int(id_karyawan))
        result = conn.execute(querycheck).fetchall()
        converted_array = [d[0].strftime('%Y-%m-%d') for d in result]
        tgl_perbantuan = json.loads(tgl_perbantuan_str)
        formatted_dates = [dt.strptime(date, '%Y-%m-%d').strftime('%Y-%m-%d') for date in tgl_perbantuan]
        common_elements = [element for element in formatted_dates if element in converted_array]


        if common_elements :    
            response = jsonify({"error": common_elements })
            response.status_code = 400
            return response
        else :
            for date_str in tgl_perbantuan:        
                query = text("INSERT INTO user_temp (id_karyawan,id_cabang_baru,tgl_perbantuan,jabatan_temp) VALUES (:id_karyawan, :id_cabang_baru, '" + date_str + "' ,:jabatan_temp)")
                query  = query.bindparams(id_karyawan=int(id_karyawan), id_cabang_baru=int(id_cabang_baru), jabatan_temp=int(jabatan_temp)  )
                result = conn.execute(query)      
                conn.commit()

            # get user temp
                getuser = text("SELECT id FROM user_temp WHERE id_karyawan=:id_karyawan AND tgl_perbantuan='"+date_str+"' ")
                querycheck  = getuser.bindparams(id_karyawan=int(id_karyawan))
                resultcheck = conn.execute(querycheck).fetchone()
                for i in range(len(json.loads(FiturPilihan))):
                    query2 = text("INSERT INTO hakakses_temp (id_karyawan,id_fitur,id_usertemp) VALUES (:id_karyawan, :id_fitur, :id_usertemp)")
                    query2  = query2.bindparams(id_karyawan=int(id_karyawan), id_fitur=int(json.loads(FiturPilihan)[i]), id_usertemp=int(resultcheck[0])  )
                    result2 = conn.execute(query2)      
                    conn.commit()  

        return jsonify({"msg": "Success Add Perbantuan"  })


@app.route('/getallkaryawan')
# @token_auth.login_required
def extra41():

    '''
        Mendapatkan List Fitur berdasarkan Id User.
        @result `dict` List Fitur join Users Akses.
    '''

    with db.connect() as conn:


        id_cabang  = request.form.get('id_cabang')     

        query = text("SELECT karyawan.* from karyawan WHERE id_cabang =:id_cabang ")
        query  = query.bindparams(id_cabang=int(id_cabang))
        result = conn.execute(query).fetchall()
        result = set(result) if result or len(list(result)) == 0 else abort(HTTPException)
        return result

@app.route('/api/extra/imageoperasional', methods=['POST'])
# @token_auth.login_required
def extra42():


    with db.connect() as conn:

        id_AktivitasAset  = request.json.get('id_AktivitasAset')     
        foto_operasional  = request.json.get('foto_operasional')     
        query = ''

        for i in range(len(foto_operasional)):
            query = text("INSERT INTO detail_aktivitas_aset (id_aktivitasaset, foto_operasional) VALUES (:id_aktivitasaset, :foto_operasional) ")
            query  = query.bindparams(id_aktivitasaset=int(id_AktivitasAset), foto_operasional=foto_operasional[i])
            result = conn.execute(query)      
            conn.commit()  

        result = set({"message" : "Data Successfully Added", "status" : " 200"}) if result or len(list(result)) == 0 else abort(HTTPException)
        return result

@app.route('/api/extra/loghistory', methods=['POST'])
# @token_auth.login_required
def extra43():


    with db.connect() as conn:


        waktu_mulai  = request.form.get('waktu_mulai')    
        waktu_selesai  = request.form.get('waktu_selesai')  
        m = dt.strptime(waktu_mulai, '%Y-%m-%dT%H:%M:%S.%fZ')
        s = dt.strptime(waktu_selesai, '%Y-%m-%dT%H:%M:%S.%fZ')

        query = text("""
                    SELECT karyawan.username, aktivitas.nama_aktivitas, aset.nama_awal, aset.id as id_aset, task.tanggal_operasional, task.status
                    FROM task 
                    LEFT JOIN aktivitas_aset ON task.id_aktivitasaset = aktivitas_aset.id 
                    LEFT JOIN aset ON aktivitas_aset.id_aset = aset.id 
                    LEFT JOIN aktivitas ON aktivitas_aset.id_aktivitas = aktivitas.id 
                    LEFT JOIN karyawan ON task.status = karyawan.id 
                    WHERE waktu_mulai BETWEEN :waktu_mulai AND :waktu_selesai 
                    AND waktu_sampai BETWEEN :waktu_mulai AND :waktu_selesai
                """)

        query  = query.bindparams(waktu_mulai=m, waktu_selesai=s)
        result = conn.execute(query).fetchall()

        result = set(result) if result or len(list(result)) == 0 else abort(HTTPException)
        return result



@app.route('/api/extra/deleteuserperbantuan', methods=['DELETE'])
# @token_auth.login_required
def extra44():


    with db.connect() as conn:

        id    = request.args.get('id')
        id    = re.sub('[^A-Za-z0-9 -]+', '', id)  
        query = text("DELETE FROM user_temp WHERE id = :id ")
        query  = query.bindparams(id=int(id))
        result = conn.execute(query)
        conn.commit() 

        query = text("DELETE FROM hakakses_temp WHERE id_usertemp = :id_usertemp ")
        query  = query.bindparams(id_usertemp=int(id))
        result = conn.execute(query)
        conn.commit() 

        result = set({"message" : "Data Successfully Deleted", "status" : " 200"}) if result or len(list(result)) == 0 else abort(HTTPException)
        return result

@app.route('/api/extra/checkstatustask')
# @token_auth.login_required
def extra45():


    with db.connect() as conn:            
        id    = request.args.get('id')
        id    = re.sub('[^A-Za-z0-9 -]+', '', id)  
        query = text("SELECT * FROM task WHERE id_karyawan = :id AND status = 1 AND waktu_sampai < :current_time  ")
        query = query.bindparams(id=int(id), current_time=dt.now())
        result = conn.execute(query).fetchall()
        result = set(result) if result or len(list(result)) == 0 else abort(HTTPException)
        return result



@app.route('/api/extra/kelasgruptext')
# @token_auth.login_required
def extra46():

    with db.connect() as conn:
        query = text("""
        SELECT id, nama_kelas as text
        FROM kelas_grup_aset
               """)  
        result = conn.execute(query).fetchall()
        result = set(result) if result or len(list(result)) == 0 else abort(HTTPException)
        return result

# @app.route('/api/extra/getCabang')
# # # @token_auth.login_required
# def extra47():
#     '''
#         Mendapatkan List Cabang.
#         @result `dict` List Cabang.
#     '''

#     with db.connect() as conn:

#         query = """
#             select c.id, c.nama_cabang, k.id_cabang, k.username 
#             from cabang as c
#                 left join karyawan as k on c.id=k.id_cabang
#         """

#         result = conn.execute(text(query)).fetchall()
#         result = set(result) if result else abort(HTTPException)

        return result
@app.route('/api/extra/getCabang')
# # @token_auth.login_required
def extra47():
    '''
        Mendapatkan List Cabang.
        @result `dict` List Cabang.
    '''

    with db.connect() as conn:

        query = """
            SELECT l.id, l.username, l.status, l.hp, l.email,  l.nik, l.id_cabang,
              c.nama_cabang AS cabang
            FROM karyawan AS l
                LEFT JOIN cabang AS c ON l.id_cabang = c.id
            ORDER BY l.username ASC
        """

        result = conn.execute(text(query)).fetchall()
        result = set(result) if result else abort(HTTPException)

        return result

# ---------------------------------------------------------------------------------
# MOBILE API
# ---------------------------------------------------------------------------------

@app.route('/api/mobile/getSalesProfile')
def mobile1():
    '''
        Mendapatkan Data User berdasarkan Id atau Mendapatkan List User dengan Jabatan Sales.
        @param `id` dari User `request.argument | query.param` (opsional).
        @result `dict` Data | List Users join Cabang dan Jabatan.
    '''

    with db.connect() as conn:

        id = request.args.get('id')
        query = ""

        if id :
            query = "WHERE users.id='" + id + "' AND users.id_jabatan='4'" # Add Where Id Field

        query = """
            SELECT users.*, 
                cabang.nama AS nama_cabang, 
                jabatan.nama AS nama_jabatan
            FROM users 
                LEFT JOIN cabang ON users.id_cabang = cabang.id 
                LEFT JOIN jabatan ON users.id_jabatan = jabatan.id 
                {query}
            ORDER BY users.id
        """.format(query = query)

        result = conn.execute(text(query)).fetchall()
        result = set(result) if result else abort(HTTPException)

        return result



# ---------------------------------------------------------------------------------

if __name__ == '__main__':
    app.run(debug=True)
